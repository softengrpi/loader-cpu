/******
Version: 2.1
Authors:
	- Luca Ardito (original work)
	- Marco Ardizzone (later fixes)
Date: April 2017

Description:
	Test C program for energy experiment.
	Include the header experiment2.h and call the fuction run_sort() for running
	the experiment.
	For details on experiment see the header file experiment.h
 
Compiling:
	gcc -O3 executor.c -o executor

Running:
	./executor <sorting_algo> <num_elements> <array_type>
	Where:
		- <sorting_algo>: 'quickC' or 'bubbleC' or 'countC' or 'mergeC'
		- <num_elements>: Number of elements in the array.
		- <array_type>: A number in range [1, 5]
			+ 1 => sorted array
			+ 2 => reverse sorted array
			+ 3 => random1
			+ 4 => random2
			+ 5 => random3

Updates:
	March - April 2017: Clearer help messages
	18/3/2016 : predefined seed for pseudo-random numbers

******/

#include "executor.h"
static const char APP_NAME[] = "executor.out";

int main(int argc, char* argv[]) {

	int number_of_items;
	int array_type;
	int sep = 0;
	long begin, end;

	if (argc != 4) {

		printf("Usage: ./%s <algorithm> <array_size> <array_order>\n", APP_NAME);
		printf("\t<algorithm>: quickC - bubbleC - countC - mergeC\n");
		printf("\t<array_size>: An unsigned integer in range [0; %d]\n", INT_MAX);
		printf("\t<array_order>: An unsigned integer between 1 and 5.\n");
		printf("\t\t1 => Sorted array\n");
		printf("\t\t2 => Reversed array\n");
		printf("\t\t3 => Random sorted 1 array\n");
		printf("\t\t4 => Random sorted 2 array\n");
		printf("\t\t5 => Random sorted 3 array\n");
		return -1;
	} else {

		number_of_items = atoi(argv[2]);
		array_type = atoi(argv[3]);

		if (number_of_items < 1) {

			printf("Number of items must be > 0\n");
			return -2;

		} else if (array_type < 1 || array_type > 5) {

			printf("0 < n < 6\nn--> 1 sorted array, 2 reverse sorted array, 3 random1, 4 random2, 5 random3\n");
			return -3;
		}
	}
	int **array = generate_numbers3(number_of_items);

	begin = current_time();
	run_sort(argv[1], number_of_items, array, --array_type, &sep);
	end = current_time();

	printf("Total execution time: %f\n",(float)((float)(end-begin)/(float)1000));

	return 0;

}
