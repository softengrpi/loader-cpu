/*******
Version: 2.1
Date:  April 2017
Authors:
    - Luca Ardito
    - Rifat Rashid
    - Marco Ardizzone

Description:
    Library for energy consumption analysis on sorting algoritm for both C and
    ASM.
    This version only support sorting algorithm for integer elements.
    Max array size is max integer supported by your system (see MAX_INT inside
    limits.h).
    For using this library add header function in the code:
        #include "experiment2.h"
    For running the experiment call run_sort(sort algorithm name, No of elements)
    function.

    Example:
        run_sort("quickC",1000); //Running C code
    Supported sorting algorithms: bubble,merge,quick,counting

    Naming in the run_sort function parameter sort algoritm name use following
    format:
        sort(lower case)+Language(Upper case)
    example: bubbleC, bubbleASM

Latest Updates:
    March - April 2017:
        + Memory for arrays is allocated based on the input array size.
        + Proper memory freeing so that on lower end devices we do not run out of
          memory.
        + Counting sort was fixed to take into account the above fixes.
    25/12/2015: commented on the function run_sort
    24/12/2015: remove section with single array functions
 ********/


#ifndef ASD
#define ASD

#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <limits.h>
#define M 1000
#define MAX 50000
#define SEED 1299827

void run_sort(char*, int, int **, int, int*);

void my_bubble_c(int*, int);
void my_merge_c(int*,int);
void my_qsort_c(int*, int, int);
void counting_sort(int*, int);

int **generate_numbers3(int);
int *clone_array(int*,int);
int **clone_array2(int**,int);
void free_array2d(int**, int);

void busy_wait(long int dur);

int compare_string(const char *, const char *);

void merge_sort_recur(int *v, int p, int r, int *aux);
void my_merge(int *v, int p, int q, int r, int *aux);

void swap (int*, int , int );
int partition (int*, int, int);
void print_time(const char*, int, int, int, clock_t, clock_t);
void print_array(int*, int);
void separator(int *);
long int current_time();
/******
 
 Function: run_sort for executing sorting algorithm with fixed sequence.
 
 Parameter: N-> for no. of elements
 sort-> Name of the sorting algorithm
 
 Description:
 
 Following function runs different sorting algorithm in sequence of
 
 S(Sleep) P(Busy) S(Sleep)  Run(Run sorting algorithm) S(Sleep)
 
 _B__               _B__
 |  |   _ Run__    |    |
 |  |   |     |    |    |
 _S_|  |_S_|     |_S__|    |_S__
 
 
 For making the system to sleep we use function usleep(500*1000) : 0.5 sec
 
 For making the system busy we use function  busy_wait(500) : 500 in milliseconds
 
 ********/


void run_sort(char* sort, int n, int **array2d, int k, int *sep) {
    
    int **array2d_temp = clone_array2(array2d,n); // initialize the array
    
    int i;
    
    clock_t begin, end;
        
    for (i=0;i<30;i++) { // Loop for 30 times
        if (compare_string(sort,"quickC")) {
            begin = clock();
            
            my_qsort_c(array2d_temp[k], 0 , n-1);
            
            end = clock();

            print_time(sort, n, i, k, begin, end);            
        }
        
        if (compare_string(sort,"bubbleC")) {
                        
            begin = clock();
            
            my_bubble_c(array2d_temp[k],  n);
            
            end = clock();
            print_time(sort, n, i, k, begin, end);
            
            
        }
        
        if (compare_string(sort,"countC")) {
                        
            begin = clock();
            counting_sort(array2d_temp[k], n);
            
            end = clock();
            print_time(sort, n, i, k, begin, end);
            
        }
        
        if (compare_string(sort,"mergeC")) {            
            begin = clock();
            
            my_merge_c(array2d_temp[k],n);
            
            end = clock();
            print_time(sort, n, i, k, begin, end);
        }
        free_array2d(array2d_temp, n);
        array2d_temp = clone_array2(array2d,n); // for re-initialize the sorting elements        
    }
    
}

/**********************Utility functions***************************/

void separator(int *sep) {
    
    usleep(500*1000);
    
    busy_wait(500); // To make system busy for 1 sec
    
    
    usleep(500*1000);
    *sep = *sep + 1;
    
}

void print_time(const char *sort, int N, int i, int k, clock_t begin,clock_t end) {
    
    printf("%d;%f\n", i+1, (double)(end-begin)/(CLOCKS_PER_SEC));
    
}

long int current_time() {
    
    struct timeval tp;
    
    gettimeofday(&tp,NULL);
    
    long int ms = tp.tv_sec * 1000 + tp.tv_usec / 1000;
    
    return ms;
    
}

void busy_wait(long int dur) {
    
    long int temp=current_time()+dur;
    
    while(temp>current_time());
    
}

int compare_string(const char *first, const char *second) {
    
    while (*first == *second) {
        
        if (*first == '\0' || *second == '\0')
            break;
        
        first++;
        second++;
        
    }
    
    if (*first == '\0' && *second == '\0')
        return 1;
    else
        return 0;
    
}

int **clone_array2(int **array, int size) {
    
    int i,j;
    int **res = (int**) malloc(size* sizeof(int*));
    
    for (i = 0 ; i < 5; i++) {
        
        res[i]=(int *) malloc(sizeof(int)*size);
        
        for(j = 0 ; j < size; j++)
            res[i][j] = array[i][j];
        
    }
    
    return res;
    
}

void free_array2d(int **array, int size){
    int i;
    for (i = 0; i < 5; ++i) {
        free(array[i]);
    }
    free(array);
}

int *clone_array(int *array, int size) {
    
    int i = 0;
    int *res = (int*) malloc(size* sizeof(int));
    
    for(i=0; i<size ; i++)
        res[i] = array[i];
    
    return res;
    
}

int **generate_numbers3(int itemN) {
    
    int i, j;
    
    //4 is the number of arrays we want to create
    int **array = (int**) malloc(4 * sizeof(int*));

    //FIRST ARRAY IS SORTED
    array[0] = (int*) malloc(sizeof(int) * itemN);
    for (i = 0; i < itemN; i++) {
            array[0][i] = i;
    }
    //SECOND ARRAY IS REVERSED
    array[1] = (int*) malloc(sizeof(int) * itemN);
    
    for (i = 0, j = itemN - 1; i < itemN; i++, j--) {
        if (i < M)
            array[1][i] = j;
        else
            array[1][i] = 0;
    }
    
    srand(SEED);
    
    for (i = 2 ; i < 5; i++) {
        
        array[i] = (int *) malloc(sizeof(int) * itemN);
        
        for(j = 0 ; j < itemN; j++)
            array[i][j] = rand() % (itemN-1);
        
    }

    return array;
    
}

void print_array(int *array, int size) {
    int i;
    for(i=0; i < size; ++i)
        printf("%d\t", array[i]);
    printf("\n");
}


/***********************Sorting Algorithms***************************************/


void my_bubble_c(int array[], int n) {
    
    int i, j, temp, flag;
    
    flag = 1;
    
    for (i=0; i<n-1 && flag==1; i++) {
        
        flag = 0;
        
        for (j = 0; j < n-1-i; j++)
            if (array[j] > array[j+1]) {
                
                flag = 1;
                temp = array[j];
                array[j] = array[j+1];
                array[j+1] = temp;
                
            }
        
    }
    
}

void my_merge_c(int *v, int dim) {
    
    int *aux;
    
    aux = (int *) malloc (dim * sizeof(int));
    
    merge_sort_recur(v,0, dim-1, aux);
    
}

void merge_sort_recur(int *v, int p, int r, int *aux) {
    
    int q;
    
    if (p < r) {
        
        q = (p+r)/2;
        
        merge_sort_recur(v,p,q,aux);
        merge_sort_recur(v,q+1,r,aux);
        my_merge(v,p,q,r,aux);
        
    }
    
}
void my_merge(int *v, int p, int q, int r, int *aux) {
    
    int i, j, k;
    
    for ( i=p, j = q+1, k = p; i<=q && j<=r; ) {
        
        if (v[i] < v[j] )
            aux[k++] = v[i++];
        else
            aux[k++] = v[j++];
        
    }
    
    while (i <= q)
        aux[k++] = v[i++];
    
    while (j<=r)
        aux[k++] = v[j++];
    
    for(k=p; k<=r; k++)
        v[k] = aux[k];
    
}

void swap (int *a, int left, int right) {
    
    int temp;
    temp=a[left];
    a[left]=a[right];
    a[right]=temp;
    
}

void my_qsort_c( int *a, int low, int high ) {
    
    int pivot;
    
    if ( high > low ) {        
        pivot = partition( a, low, high );
        my_qsort_c( a, low, pivot-1 );
        my_qsort_c( a, pivot+1, high );
    }
}

int partition(int *a, int low, int high) {
    
    int left, right;
    int pivot_item;
    
    left = low;
    pivot_item = a[low];
    right = high;
    
    while ( left < right ) {
        
        while( a[left] <= pivot_item )
            left++;
        
        while( a[right] > pivot_item )
            right--;
        
        if ( left < right )
            swap(a,left,right);
        
    }
    
    a[low] = a[right];
    a[right] = pivot_item;
    
    return right;
    
}

void counting_sort(int A[], int n) {
    
    int i, j=0;
    int* C;
    int min = A[0];
    int max = A[0];

    //Calculate min and max elements
    for(i=1; i < n; ++i){
        if (A[i] > max)
            max = A[i];
        else if(A[i] < min)
            min = A[i];
    }

    //Make an array that is max-min+1 in size
    //Also, initialize the array to 0
    C = (int*) calloc(max - min + 1, sizeof(int));
    for(i=0; i < n; ++i){
        C[A[i] - min]++;
    }

    for(i=0; i < max - min + 1; ++i){
        while(C[i] > 0){
            A[j] = i + min;
            j++;
            C[i]--;
        }
    }
    free(C);
}

#endif
